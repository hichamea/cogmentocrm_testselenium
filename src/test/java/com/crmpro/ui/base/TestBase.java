package com.crmpro.ui.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


import io.github.bonigarcia.wdm.WebDriverManager;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static ExtentReports extent;
	public static ExtentTest Logger;
//	public static EventFiringWebDriver e_driver;
//	public static WebListener webListener;
//  public static ATUTestRecorder recoder;
	
	public TestBase() throws IOException{
		prop = new Properties();
		FileInputStream fis = new FileInputStream("src\\test\\java\\com\\crmpro\\ui\\config\\config.properties");
		prop.load(fis);
	}
	
	public void initialization(String browser){
		
		if (prop.getProperty("proxy") != null){
			if (browser.equalsIgnoreCase("Firefox")){
				WebDriverManager.firefoxdriver().proxy(prop.getProperty("proxy")).setup();
				driver = new FirefoxDriver();
			}else if (browser.equalsIgnoreCase("Chrome")) {
				WebDriverManager.chromedriver().proxy(prop.getProperty("proxy")).setup();
				driver = new ChromeDriver();
			}
		}else{
			if (browser.equalsIgnoreCase("Firefox")){
				WebDriverManager.firefoxdriver().proxy(prop.getProperty("proxy")).setup();
				driver = new FirefoxDriver();
			}else if (browser.equalsIgnoreCase("Chrome")) {
				WebDriverManager.chromedriver().proxy(prop.getProperty("proxy")).setup();
				driver = new ChromeDriver();
			}
		}
		
		
		
		// e_driver = new EventFiringWebDriver(driver);
		// webListener = new WebListener();
		// e_driver.register(webListener);
		// driver = e_driver;
		
		driver.get(prop.getProperty("URL"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
