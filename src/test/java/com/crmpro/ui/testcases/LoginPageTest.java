package com.crmpro.ui.testcases;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.crmpro.ui.base.TestBase;
import com.crmpro.ui.pages.HomePage;
import com.crmpro.ui.pages.LoginPage;
import com.crmpro.ui.util.TestUtils;
import com.relevantcodes.extentreports.LogStatus;

@Test public class LoginPageTest extends TestBase{

	
	/*
	 * 
	 * Deux types d'assertion :
	 * 1- Hard assertion
	 * 2- Soft assertion
	 * 
	 */
	
	/*
	 * @BeforeClass une seule fois avant toutes les classes
	 * @AfterClass une seule fois apr�s tous les cours
	 * @BeforeMethod & @AfterMethod avant et apr�s chaque m�thode
	 * @BeforeTest & @AfterTest une seule fois avant et apr�s Test
	 * @BeforeSuite & @AfterSuite une seule fois avant et apr�s Suite
	 */
	
	LoginPage loginPage;
	HomePage homePage;
	//ATUTestRecorder recoder;
	
	public LoginPageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser, Method method) throws IOException{
		Logger = extent.startTest(method.getName());
		initialization(browser);
		loginPage = new LoginPage();
	//	recoder = new ATUTestRecorder("C:\\Users\\A747995\\workspace\\CogmentoCRM\\TestReport",method.getName(),false);
	//	recoder.start();
	}
	
	@AfterMethod
	public void tearDown(Method method,ITestResult result) throws IOException {

		TestUtils.takeSnapshot(method.getName());
		//recoder.stop();
		
		if (result.getStatus() == ITestResult.SUCCESS) {
			
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Capture</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Video</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+ result.getName()+".txt" +"'><span class='lable info'>Console Logs</span></a>");
		
		}else if ((result.getStatus() == ITestResult.FAILURE)) {
			
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Capture</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Video</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+ result.getName()+".txt" +"'><span class='lable info'>Console Logs</span></a>");
		
		}else {
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Capture</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+result.getName()+".mov'><span class='lable info'>Voir Video</span></a>");
			
			Logger.log(LogStatus.PASS, "<a href='"+ result.getName()+".txt" +"'><span class='lable info'>Console Logs</span></a>");
		}
		
		driver.quit();
	}
	
	@Test (priority=1)
	public void titleTest(Method method) throws IOException{
		homePage = loginPage.performLogin();
		String expectedResult = "Cogmento CRM";
		String actualResult = homePage.getTitle();
		System.out.println(actualResult);
		Assert.assertEquals(actualResult, expectedResult, "Le titre n'est pas correct");
		TestUtils.takeSnapshot(method.getName());
	}
	
	@Test (priority=2)
	public void urlCheck(){
		String expectedResult = "https://ui.freecrm.com/";
		String actualResult = loginPage.getURL();
		System.out.println(actualResult);
		Assert.assertEquals(actualResult, expectedResult,"L'URL n'est pas correcte");
	}
	
	@Test (priority=3)
	public void testLogo(){
		boolean actualResult = loginPage.loginIsDisplayed();
		System.out.println(actualResult);
		Assert.assertTrue(actualResult,"Le logo n'est pas affich�");
	}
	
	@Test (priority=4)
	public void loginTest() throws IOException{
		
		homePage = loginPage.performLogin();
		String expectedResult = "Hicham EA";
		String actualUser = homePage.getActualUser();
		
		Assert.assertEquals(actualUser, expectedResult,"Le nom d'utilisateur ou le mot de passe est erron�");
	}
	
	
}
