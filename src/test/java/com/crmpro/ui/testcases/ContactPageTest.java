package com.crmpro.ui.testcases;


import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.crmpro.ui.base.TestBase;
import com.crmpro.ui.pages.ContactPage;
import com.crmpro.ui.pages.HomePage;
import com.crmpro.ui.pages.LoginPage;
import com.crmpro.ui.util.TestUtils;
import com.relevantcodes.extentreports.LogStatus;

public class ContactPageTest extends TestBase{

	LoginPage loginPage;
	HomePage homePage;
	ContactPage contactPage;
	
	public ContactPageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	
	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser, Method method) throws IOException{
		Logger = extent.startTest(method.getName());
		initialization(browser);
		loginPage = new LoginPage();
		homePage = loginPage.performLogin();
	}
	
	@AfterMethod
	public void tearDown(ITestResult result){
		
		if (result.getStatus() == ITestResult.SUCCESS) {
			Logger.log(LogStatus.PASS, "Test pass");
		}else if ((result.getStatus() == ITestResult.FAILURE)) {
			Logger.log(LogStatus.FAIL, "Test failled");
		}else {
			Logger.log(LogStatus.SKIP, "Test skipped");
		}
		
		driver.quit();
	}
	
	@Test(dataProvider="testData")
	public void addContact(String fName, String lName, String companyName, String departementName) throws IOException{
		contactPage = homePage.clickOnNewContact();
		contactPage.addNewContact(fName, lName, companyName, departementName);	
	}
	
	@DataProvider
	public Object[][] testData() throws IOException{
		Object data[][] = TestUtils.getDataFromExcel("ContactPage");
		return data;
	}

}
