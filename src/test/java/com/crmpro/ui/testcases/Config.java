package com.crmpro.ui.testcases;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.crmpro.ui.base.TestBase;
import com.relevantcodes.extentreports.ExtentReports;

public class Config extends TestBase{

	public Config() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@BeforeSuite
	public void start(){
		extent = new ExtentReports("TestReport\\index.html", true);
		extent.addSystemInfo("OS","Windows");
		extent.addSystemInfo("Owner","Hicham EA");
		extent.addSystemInfo("Test name","Free CRM");
	}
	
	@AfterSuite
	public void end(){
		extent.flush();
	}

}
