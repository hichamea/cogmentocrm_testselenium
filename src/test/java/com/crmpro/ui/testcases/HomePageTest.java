package com.crmpro.ui.testcases;

import java.io.IOException;





import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.crmpro.ui.base.TestBase;
import com.crmpro.ui.pages.HomePage;
import com.crmpro.ui.pages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class HomePageTest extends TestBase{

	LoginPage loginPage;
	HomePage homePage ;
	
	public HomePageTest() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod
	@Parameters("browser")
	public void setUp(String browser, Method method) throws IOException{
		Logger = extent.startTest(method.getName());
		initialization(browser);
		loginPage = new LoginPage();
		homePage = loginPage.performLogin();
	}
	
	@AfterMethod
	public void tearDown(ITestResult result){
		
		if (result.getStatus() == ITestResult.SUCCESS) {
			Logger.log(LogStatus.PASS, "Test pass");
		}else if ((result.getStatus() == ITestResult.FAILURE)) {
			Logger.log(LogStatus.FAIL, "Test failled");
		}else {
			Logger.log(LogStatus.SKIP, "Test skipped");
		}
		
		driver.quit();
	}
	
	@Test (priority=1)
	public void clickOnContactsTest(){
		homePage.clickOnContacts();
		boolean actualResult = homePage.isContactsLableDisplayed();	
		Assert.assertTrue(actualResult, "Le Libelle Contacts n'est pas affiché");
	}
	
	@Test (priority=2)
	public void clickOnDealsTest(){
		homePage.clickOnDeals();
		boolean actualResult = homePage.isDealsLableDisplayed();
		Assert.assertTrue(actualResult, "Le Libelle Deals n'est pas affiché");
	}
	
	@Test (priority=3)
	public void clickOnTasksTest(){
		homePage.clickOnTasks();
		boolean actualResult = homePage.isTasksLableDisplayed();
		Assert.assertTrue(actualResult, "Le Libelle Tasks n'est pas affiché");
	}
	

}
