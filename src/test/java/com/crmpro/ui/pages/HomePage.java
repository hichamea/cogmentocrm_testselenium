package com.crmpro.ui.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crmpro.ui.base.TestBase;

public class HomePage extends TestBase{

	public HomePage() throws IOException {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[contains(text(),'Contacts')]")
	WebElement contact;
	
	@FindBy(xpath="//div[text()='Contacts']")
	WebElement contactsLable;
	
	@FindBy(xpath="//span[contains(text(),'Deals')]")
	WebElement deals;
	
	@FindBy(xpath="//div[text()='Deals']")
	WebElement dealsLable;
	
	@FindBy(xpath="//span[contains(text(),'Tasks')]")
	WebElement tasks;
	
	@FindBy(xpath="//div[text()='Tasks']")
	WebElement tasksLable;
	
	@FindBy(xpath="/html[1]/body[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/a[1]/button[1]")
	WebElement newContact;
	
	@FindBy(xpath="//span[@class='user-display']")
	WebElement actualUser;
	
	public ContactPage clickOnNewContact() throws IOException{
		contact.click();
		driver.navigate().refresh();
		newContact.click();
		return new ContactPage();
	}
	public String getTitle(){
		return driver.getTitle();
	}
	
	public String getActualUser(){
		return actualUser.getText();
	}
	
	public void clickOnContacts(){
		contact.click();
	}
	
	public boolean isContactsLableDisplayed(){
		return contactsLable.isDisplayed();
	}
	
	public void clickOnDeals(){
		deals.click();
	}
	
	public boolean isDealsLableDisplayed(){
		return dealsLable.isDisplayed();
	}
	
	public void clickOnTasks(){
		tasks.click();
	}
	
	public boolean isTasksLableDisplayed(){
		return tasksLable.isDisplayed();
	}
	
}
