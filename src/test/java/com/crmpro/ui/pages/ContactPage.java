package com.crmpro.ui.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crmpro.ui.base.TestBase;

public class ContactPage extends TestBase{

	public ContactPage() throws IOException {
		PageFactory.initElements(driver, this);
	}

	@FindBy(name="first_name")
	WebElement firstName;
	
	@FindBy(name="last_name")
	WebElement lastName;
	
	@FindBy(name="//div[@name='company']//input[@class='search']")
	WebElement company;
	
	@FindBy(name="department")
	WebElement department;
	
	@FindBy(xpath="//button[@class='ui linkedin button']")
	WebElement save;
	
	public void addNewContact(String fName, String lName, String companyName, String departementName){
		firstName.sendKeys(fName);
		lastName.sendKeys(lName);
		company.sendKeys(companyName);
		department.sendKeys(departementName);
		save.click();
	}
}
