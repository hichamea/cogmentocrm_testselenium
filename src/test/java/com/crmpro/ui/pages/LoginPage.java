package com.crmpro.ui.pages;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.crmpro.ui.base.TestBase;

public class LoginPage extends TestBase{

	public LoginPage() throws IOException {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//img[@src='https://d3lh3kd7bj2evy.cloudfront.net/img/logo.png']")
	WebElement logo;
	
	@FindBy(name="email")
	WebElement emailText;
	
	@FindBy(name="password")
	WebElement passwordText;
	
	@FindBy(xpath="//div[@class='ui fluid large blue submit button']")
	WebElement loginButton;
	
	public String getTitle(){
		String actualTitle = driver.getTitle();
		return actualTitle;
	}
	
	public String getURL(){
		String actualURL = driver.getCurrentUrl();
		return actualURL;
	}
	
	public boolean loginIsDisplayed(){
		return logo.isDisplayed();
	}
	
	public HomePage performLogin() throws IOException{
		emailText.sendKeys(prop.getProperty("email"));
		passwordText.sendKeys(prop.getProperty("password"));
		loginButton.click();
		return new HomePage();
	}
	
}
