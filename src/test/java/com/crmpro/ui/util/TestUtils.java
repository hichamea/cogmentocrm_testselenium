package com.crmpro.ui.util;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;


import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.crmpro.ui.base.TestBase;

public class TestUtils extends TestBase{

	public TestUtils() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void takeSnapshot(String name) throws IOException{
		File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFile, new File("TestReport\\"+name+".png"));
	}
	
	public static Object[][] getDataFromExcel(String sheetName) throws IOException{
		
			File file = new File("testData.xlsx");
			FileInputStream fis = new FileInputStream(file);
			
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			Sheet sheet = workbook.getSheet(sheetName);
			
			int rows = sheet.getLastRowNum();
			int columns = sheet.getRow(0).getLastCellNum();
			
			Object data[][] = new Object[rows][columns];
			
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < columns; j++) {
					data[i][j] = sheet.getRow(i).getCell(j).toString();
				}
			}
			workbook.close();
			return data;	
	}
}
